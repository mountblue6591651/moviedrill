function getMovieByActor(data){
    let movies = Object.keys(data);
    
    let result = movies.reduce((acc, movie) => {
        if(data[movie].actors.includes('Leonardo Dicaprio')){
            acc[movie] = data[movie];
        }
        return acc;
    }, {});
    return result;
}

module.exports = getMovieByActor;