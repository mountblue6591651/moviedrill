function getMovieByNomination(data) {
    let result = [];
    for(key in data){
        if(data[key].oscarNominations > 3){
            if(Number(data[key].totalEarnings.replace('$','').replace('M','')) > 500){
                result.push({[key]:data[key]});
            }
        }
    }
    return result;
}

module.exports = getMovieByNomination;