function sortMovies(data) {
  let movies = Object.keys(data);

  let result = movies
    .sort((movie1, movie2) => {
      if (data[movie1].imdbRating < data[movie2].imdbRating) {
        return 1;
      } else if (data[movie1].imdbRating > data[movie2].imdbRating) {
        return -1;
      } else {
        let earning1 = data[movie1].totalEarnings
          .replace("$", "")
          .replace("M", "");
        let earning2 = data[movie2].totalEarnings
          .replace("$", "")
          .replace("M", "");
        if (earning1 < earning2) {
          return 1;
        } else if (earning1 > earning2) {
          return -1;
        }
      }
    })
    .reduce((acc, movie) => {
      acc[movie] = data[movie];
      return acc;
    }, {});

  return result;
}

module.exports = sortMovies;
