function getMovieByEarning(data, earning) {
  let movies = Object.keys(data);

  let result = movies
    .filter((movie) => {
      let earned = Number(
        data[movie].totalEarnings.replace("$", "").replace("M", "")
      );
      if (earned > earning) {
        return true;
      }
    })
    .reduce((acc, movie) => {
      acc[movie] = data[movie];
      return acc;
    }, {});
  return result;
}

module.exports = getMovieByEarning;
