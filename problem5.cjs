function groupMovies(data) {
  let movies = Object.keys(data);

  let result = movies.reduce(
    (acc, movie) => {
      if (data[movie].genre.includes("drama")) {
        acc["drama"].push({ [movie]: data[movie] });
      } else if (data[movie].genre.includes("sci-fi")) {
        acc["sci-fi"].push({ [movie]: data[movie] });
      } else if (data[movie].genre.includes("adventure")) {
        acc["adventure"].push({ [movie]: data[movie] });
      } else if (data[movie].genre.includes("thriller")) {
        acc["thriller"].push({ [movie]: data[movie] });
      } else if (data[movie].genre.includes("crime")) {
        acc["crime"].push({ [movie]: data[movie] });
      }
      return acc;
    },
    { drama: [], "sci-fi": [], adventure: [], thriller: [], crime: [] }
  );
  return result;
}

module.exports = groupMovies;
